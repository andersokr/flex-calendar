//Copyright FBS
numrows = 0;
inputdata = new Array();
selecteddate = "";
datex = new Date();
day = datex.getDate();
month = datex.getMonth();
year = datex.getFullYear();
months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

function datatype(key) {
    this.key = key;
    this.avalues = new Array(12);
}

function calendar(dir) {
    if (dir == null) {
        var this_month = new Date(year, month, 1);
        var first_week_day = this_month.getDay();
        startdate = new Date(this_month - (first_week_day * 86400000));
    } else if (dir == "u") {
        startdate.setMilliseconds(startdate.getMilliseconds() - (7 * 86400000));
    } else if (dir == "d") {
        startdate.setMilliseconds(startdate.getMilliseconds() + (7 * 86400000));
    }
    else if(dir == "u-m" || dir == "d-m"){
        handleMonthChange(dir);
    }
    var begdate = new Date(startdate);
    var calendar_html = '<table width="100%" height="100%" align="center" cellpadding=0 cellspacing=0 class="xout">';
    calendar_html += '<tr><th class="xinth dayf" height=1 width="13%">Sunday</th><th class="xinth dayf" height=1 width="14%">Monday</th><th class="xinth dayf" height=1 width="15%">Tuesday</th><th class="xinth dayf" height=1 width="16%">Wednesday</th><th class="xinth dayf" height=1 width="15%">Thursday</th><th class="xinth dayf" height=1 width="14%">Friday</th><th class="xinth dayf" height=1 width="13%">Saturday</th></tr>';
    // Currently the calendar defaults to 6 weeks.  
    // Make it smarter to show 5 or 4 weeks, whatever the max is need to show the whole month:
    // get total number of days in the month
    if(numrows == 0){
        numrows = calculateNumrows();
    }
    else if(dir == 'rf'){
        numrows = document.getElementsByName('rowsize')[0].value;
    }
    for (var row = 1; row <= numrows; row++) {
        calendar_html += '<tr>';
        for (var col = 1; col <= 7; col++) {
            var xclass = (begdate.getMonth() - month) % 2 != 0 ? "xin" : "xinalt";
            var keydate = (begdate.getMonth() + 1) + '-' + begdate.getDate() + '-' + begdate.getFullYear();
            var tent = "";
            var fcnt = 0;
            for (var a = 0; a < inputdata.length; a++) {
                if (inputdata[a].key == keydate) {
                    xclass = "xinaltsel";
                    fcnt = 0;
                    if(inputdata[a].avalues[25]){
                        tent += "<br><b>" + countAllDayEvents(inputdata[a].avalues[25])+ "</b><br>";
                    }
                    for (var b = 1; b <= 24; b++) {
                        if (inputdata[a].avalues[b - 1] != null && inputdata[a].avalues[b - 1] != "" && fcnt < 3) {
                            var z = (b <= 12 ? b : (b - 12)) + ":00 " + (b < 12 ? "am" : (b != 24 ? "pm" : "am"));
                            tent += "<br>" + z;
                            fcnt++;
                        }
                    }
                }
            }
            if (tent != "") {
                tent = "<br>" + tent + (fcnt == 3 ? "<br>..." : "");
            }
            var highlight = false;
            if (day == begdate.getDate() && month == begdate.getMonth() && year == begdate.getFullYear()) highlight = true;
            calendar_html += '<td class="numf" style="border-style:solid; border-width:1px;" height="' + Math.floor(100 / numrows) + '%" onMouseUp=\'divw("<b>' + months[begdate.getMonth()] + ' ' + begdate.getDate() + ',' + begdate.getFullYear() + '</b>","dtselected");selecteddate="' + keydate + '";loadinputs();\' class="' + xclass + '" valign="top" style="cursor: pointer;">' + (highlight ? "<b>" : "") + (begdate.getDate() == 1 ? (months[begdate.getMonth()] + " ") : "") + begdate.getDate() + (highlight ? "</b>" : "") + tent + '</td>';
            begdate.setMilliseconds(begdate.getMilliseconds() + 86400000);
        }
        calendar_html += '</tr>';
    }
    calendar_html += '</table>';
    return (calendar_html);
}

function handleMonthChange(dir){
    if (dir == "u-m") {
        var startDay = startdate.getDate();
        if(startDay > 1){
            // go to the first day of that month
            var monthStart = new Date(startdate.getFullYear(), startdate.getMonth(), 1);
            var first_week_day = monthStart.getDay(); 
            startdate = new Date(monthStart - (first_week_day * 86400000));
        }
        else if (startDay === 1){
            // go to the first day of the previous month
            var tMonth = startdate.getMonth();
            var tYear = startdate.getFullYear(); 
            // what if month is january?
            if(tMonth == 0){
                tMonth = 11;
                tYear--;
            }
            else{
                tMonth--;
            }
            var monthStart = new Date(tYear, tMonth, 1);
            var first_week_day = monthStart.getDay(); 
            startdate = new Date(monthStart - (first_week_day * 86400000));
        }
    }
    else if (dir == "d-m") {
        var startDay = startdate.getDate();
        if(startDay > 1){
            // go 2 months forward and get the first day
            var tMonth = startdate.getMonth();
            var tYear = startdate.getFullYear(); 
            // what if startsate.getMonth is december?
            if(tMonth == 11){
                tMonth = 1;
                tYear++;
            }
            // what if startsate.getMonth is november?
            else if(tMonth == 10){
                tMonth = 0;
                tYear++;
            }
            else{
                tMonth += 2;
            }
            var monthStart = new Date(tYear, tMonth, 1);
            var first_week_day = monthStart.getDay(); 
            startdate = new Date(monthStart - (first_week_day * 86400000));
        }
        else if(startDay === 1){
            // go 1 month forward and get the first day
            var tMonth = startdate.getMonth();
            var tYear = startdate.getFullYear();
            // what if startsate.getMonth is december?
            if(tMonth == 11){
                tMonth = 0;
                tYear++;
            }
            else{
                tMonth ++;
            }
            var monthStart = new Date(tYear, tMonth, 1);
            var first_week_day = monthStart.getDay(); 
            startdate = new Date(monthStart - (first_week_day * 86400000));
        }
    }
}

function calculateNumrows(){
    var nextMonth = new Date(year, month + 1, 1);
    var totalDaysThisMonth = new Date(nextMonth - (1 * 86400000)).getDate();
    var totalDaysDisplayed = totalDaysThisMonth + new Date(year, month, 1).getDay();
    return(Math.ceil(totalDaysDisplayed / 7));
}

function loadinputs() {
    var temp;
    var foundkey = false;
    for (var a = 0; a < inputdata.length; a++) {
        if (inputdata[a].key == selecteddate) {
            foundkey = true;
            for (var b = 7; b <= 19; b++) {
                temp = eval("document.xform.entry" + (b));
                temp.value = (inputdata[a].avalues[b] == null ? "" : inputdata[a].avalues[b]);
            }
            if(inputdata[a].avalues[25]){
                temp = eval("document.xform.entryAllDay");
                temp.value = inputdata[a].avalues[25];
            }
        }
    }
    if (!foundkey) {
        for (var c = 7; c <= 19; c++) {
            temp = eval("document.xform.entry" + c);
            temp.value = "";
        }
        temp = eval("document.xform.entryAllDay");
        temp.value = "";
    }
}

function countAllDayEvents(xtext){
    var eventCount = 0;
    var eventList = xtext.split('\n');
        for(var event in eventList){
            if(eventList[event] != ""){
                eventCount++;
            }
        }
        
    return(eventCount > 1 ? '' + eventCount + ' All Day Events' : '1 All Day Event');
}

function savedata(xtext, xinput) {
    
    var foundkey = false;
    if (inputdata.length != 0) {
        for (var a = 0; a < inputdata.length; a++) {
            if (inputdata[a].key == selecteddate) {
                if(xinput == "entryAllDay"){
                    inputdata[a].avalues[25] = xtext;
                }
                else{
                    inputdata[a].avalues[eval(xinput.substr(5))] = xtext;
                }
                foundkey = true;
            }
        }
    }
    if (!foundkey) {
        var dt = new datatype(selecteddate);
        if(xinput != "entryAllDay"){
            dt.avalues[eval(xinput.substr(5))] = xtext;
        }
        else{
            dt.avalues[25] = xtext;
        }
        inputdata.push(dt);
    }
    var da = true;
    for (var f = 0; f < inputdata.length; f++) {
        da = true;
        for (var b = 7; b <= 19; b++) {
            if (inputdata[f].avalues[b] != "" && inputdata[f].avalues[b] != null) {
                da = false;
            }
        }
        if(inputdata[f].avalues[25] != null && inputdata[f].avalues[25] != ''){
            da = false;
        }
        if (da) {
            inputdata.splice(f, 1);
        }
    }
    divw(calendar(null), 'lc');
}

function dputs() {
    // 3. Currently we have data for hourly time slots
    // Add the ability to enter in daily entries.
    var inputs_html = '<tr><td align="right" valign="top" nowrap class="numf"><b>All Day</b> </td><td><textarea class="hwf rounded" name="entryAllDay" rows="4" cols="29" onChange="savedata(this.value,this.name);"></textarea></td></tr>';
    //6. Change the 24 hour inputs to a range only of 7am to 7pm
    for (var x = 7; x <= 19; x++) {
        var z = (x <= 12 ? x : (x - 12)) + ":00 " + (x < 12 ? "am" : (x != 24 ? "pm" : "am"));
        inputs_html += '<tr><td align="right" valign="top" nowrap class="numf"><b>' + z + '</b> </td><td><textarea class="hwf rounded" name="entry' + x + '" rows="4" cols="29" onChange="savedata(this.value,this.name);"></textarea></td></tr>';
    }
    return "<table cellpadding=1 cellspacing=0 border=0>" + inputs_html + "</table>";
}

function divw(text, id) {
    var x;
    if (document.getElementById) {
        x = document.getElementById(id);
        x.innerHTML = '';
        x.innerHTML = text;
    } else if (document.all) {
        x = document.all[id];
        x.innerHTML = text;
    } else if (document.layers) {
        x = document.layers[id];
        x.document.open();
        x.document.write(text);
        x.document.close();
    }
}

function w(m) {
    if ("undefined" != m.toString()) {
        document.writeln(m.toString());
    } else {
        document.writeln("<br>");
    }
}

function init() {
    selecteddate = (month + 1) + '-' + day + '-' + year;
    divw("<b>" + months[month] + " " + day + "," + year + "</b>", "dtselected");
    for (var c = 7; c <= 19; c++) {
        temp = eval("document.xform.entry" + c);
        temp.value = "";
    }
    //document.rowform.rowsize.value = calculateNumrows();
}

// Add a color picker to the footer of the application to allow the end user to change their basic color theme.  
// Apply the color upon selecting a new color.
function changeColorTheme(){
    var newColor = event.target.value;
    for(var i = 0; i < document.getElementsByClassName("rightcol").length; i++){
        document.getElementsByClassName("rightcol")[i].style.backgroundColor = newColor
    }
    for(var i = 0; i < document.getElementsByClassName("xinth").length; i++){
        document.getElementsByClassName("xinth")[i].style.backgroundColor = newColor
    }
    for(var i = 0; i < document.getElementsByClassName("sl").length; i++){
        document.getElementsByClassName("sl")[i].style.backgroundColor = newColor
    }
}

function handleSettingsModal(){
    document.getElementById('settings').classList.toggle("d-none");
}




